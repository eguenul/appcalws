/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appcalws.rest.resources;

import appcalws.json.LibroDiario.Comprobante;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author esteban
 */
@Path("/restLibroDiario")
public class restLibroDiario {
@POST
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.TEXT_PLAIN)
public Comprobante restLibroDiario(String objLibro) throws UnsupportedEncodingException, JAXBException {
    InputStream isjson = new ByteArrayInputStream(objLibro.getBytes("utf-8")); 
    BufferedReader br1 = new BufferedReader(new InputStreamReader(isjson));
    Gson gson2 = new Gson(); 
    Comprobante objMovimientos = gson2.fromJson(br1, Comprobante.class);
    
    JAXBContext jaxbContext = JAXBContext.newInstance(Comprobante.class);            
            /**
             * La clase Marshaller proporciona a la aplicación cliente la capacidad 
             * de convertir un árbol de contenido Java en datos XML. 
             */
    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
          
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        /*
            jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);
        */
            //Codificacion de salida
    jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");            
    jaxbMarshaller.marshal(objMovimientos, System.out);   
    
    return objMovimientos;
}    
    
    
}
