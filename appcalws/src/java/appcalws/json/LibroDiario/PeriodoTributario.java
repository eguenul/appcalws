/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appcalws.json.LibroDiario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author esteban
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {    
    "Inicial",
    "Final"  
})
@XmlRootElement(name = "PeriodoTributario")
public class PeriodoTributario {
@XmlElement(name = "Inicial", required=true)     
 private String Inicial;
@XmlElement(name = "Final", required=true) 
 private String Final;

    public String getInicial() {
        return Inicial;
    }

    public void setInicial(String Inicial) {
        this.Inicial = Inicial;
    }

    public String getFinal() {
        return Final;
    }

    public void setFinal(String Final) {
        this.Final = Final;
    }
         
 
 
    
    
}
