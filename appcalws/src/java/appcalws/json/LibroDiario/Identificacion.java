/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appcalws.json.LibroDiario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author esteban
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {    
    "RutContribuyente",
    "PeriodoTributario"

})




@XmlRootElement(name = "Identificacion")
public class Identificacion {
    @XmlElement(name = "RutContribuyente", required=true) 
    private String RutContribuyente;
    @XmlElement(name = "PeriodoTributario", required=true)
    private PeriodoTributario PeriodoTritutario;

    public String getRutContribuyente() {
        return RutContribuyente;
    }

    public void setRutContribuyente(String RutContribuyente) {
        this.RutContribuyente = RutContribuyente;
    }

    public PeriodoTributario getPeriodoTritutario() {
        return PeriodoTritutario;
    }

    public void setPeriodoTritutario(PeriodoTributario PeriodoTritutario) {
        this.PeriodoTritutario = PeriodoTritutario;
    }
    
}
