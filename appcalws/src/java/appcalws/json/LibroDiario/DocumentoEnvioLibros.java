/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appcalws.json.LibroDiario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author esteban
 */


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {    
    "RutEnvio",
    "RutContribuyente"
})
public class DocumentoEnvioLibros {
  private String RutEnvio;  
  private String RutContribuyente;
  private String RutFirma;


private String TmstFirmaEnv;

    public String getRutEnvio() {
        return RutEnvio;
    }

    public void setRutEnvio(String RutEnvio) {
        this.RutEnvio = RutEnvio;
    }

    public String getRutContribuyente() {
        return RutContribuyente;
    }

    public void setRutContribuyente(String RutContribuyente) {
        this.RutContribuyente = RutContribuyente;
    }

    public String getTmstFirmaEnv() {
        return TmstFirmaEnv;
    }

    public void setTmstFirmaEnv(String TmstFirmaEnv) {
        this.TmstFirmaEnv = TmstFirmaEnv;
    }

    public String getRutFirma() {
        return RutFirma;
    }

    public void setRutFirma(String RutFirma) {
        this.RutFirma = RutFirma;
    }
}
