/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appcalws.json.LibroDiario;

/**
 *
 * @author esteban
 */
public class Cierre {
  
	private String CantidadComprobantes;
	private String CantidadMovimientos;
	private String SumaValorComprobante;
	private String ValorAcumulado;

    public String getCantidadComprobantes() {
        return CantidadComprobantes;
    }

    public void setCantidadComprobantes(String CantidadComprobantes) {
        this.CantidadComprobantes = CantidadComprobantes;
    }

    public String getCantidadMovimientos() {
        return CantidadMovimientos;
    }

    public void setCantidadMovimientos(String CantidadMovimientos) {
        this.CantidadMovimientos = CantidadMovimientos;
    }

    public String getSumaValorComprobante() {
        return SumaValorComprobante;
    }

    public void setSumaValorComprobante(String SumaValorComprobante) {
        this.SumaValorComprobante = SumaValorComprobante;
    }

    public String getValorAcumulado() {
        return ValorAcumulado;
    }

    public void setValorAcumulado(String ValorAcumulado) {
        this.ValorAcumulado = ValorAcumulado;
    }
                                                
                                                
                                                
}
