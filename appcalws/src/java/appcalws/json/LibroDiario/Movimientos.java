package appcalws.json.LibroDiario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {    
    "CodigoCuenta",
    "Nombre",
    "TpoDocum",
    "Numero",
    "Debe",
    "Haber"    
})
@XmlRootElement(name = "Movimientos")
public class Movimientos {    
    
  @XmlElement(name = "CodigoCuenta", required = true) 
   private String CodigoCuenta;
  @XmlElement(name = "Nombre", required = true)
   private String Nombre;
  @XmlElement(name = "TpoDocum", required = true)
   private String TpoDocum;
  @XmlElement(name = "Numero", required = true)
   private String Numero;
  @XmlElement(name = "Debe", required = false)
   private String Debe;
  @XmlElement(name = "Haber", required = false)
   private String Haber;
   
   public Movimientos(){
       
       
   }

  
   
    public String getCodigoCuenta() {
        return CodigoCuenta;
    }

    public void setCodigoCuenta(String CodigoCuenta) {
        this.CodigoCuenta = CodigoCuenta;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getTpoDocum() {
        return TpoDocum;
    }

    public void setTpoDocum(String TpoDocum) {
        this.TpoDocum = TpoDocum;
    }

    public String getNumero() {
        return Numero;
    }

    public void setNumero(String Numero) {
        this.Numero = Numero;
    }

    public String getDebe() {
        return Debe;
    }

    public void setDebe(String Debe) {
        this.Debe = Debe;
    }

    public String getHaber() {
        return Haber;
    }

    public void setHaber(String Haber) {
        this.Haber = Haber;
    }

   
   
  
   
}
