/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appcalws.json.LibroDiario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author esteban
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {    
"LceDiarioRes"    
})
@XmlRootElement(name = "LceDario") 
public class LceDiario {
     @XmlElement(name = "LceDiarioRes", required = true)
     private LceDiarioRes LceDiarioREs;  

}
