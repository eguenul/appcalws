/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appcalws.json.LibroDiario;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {    
    "TpoComp",
    "NumComp",
    "FechaContable",
    "GlosaAnalisis",
    "Movimientos",
    "ValorComprobante"
})

@XmlRootElement(name = "Comprobante")
public class Comprobante {
    @XmlElement(name = "TpoComp", required=true) 
    private String TpoComp;
    @XmlElement(name = "NumComp", required=true) 
    private String NumComp;
    @XmlElement(name = "FechaContable", required=true) 
    private String FechaContable;
    @XmlElement(name = "GlosaAnalisis", required=true) 
    private String GlosaAnalisis; 
    @XmlElement(name = "ValorComprobante", required=true)  
    private String ValorComprobante; 


    
    
   @XmlElement(name = "Movimientos", required=true) 
   private ArrayList<Movimientos> Movimientos; 
 
    public String getTpoComp() {
        return TpoComp;
    }

    public void setTpoComp(String TpoComp) {
        this.TpoComp = TpoComp;
    }

    public String getNumComp() {
        return NumComp;
    }

    public void setNumComp(String NumComp) {
        this.NumComp = NumComp;
    }

    public String getFechaContable() {
        return FechaContable;
    }

    public void setFechaContable(String FechaContable) {
        this.FechaContable = FechaContable;
    }

   

    public String getGlosaAnalisis() {
        return GlosaAnalisis;
    }

    public void setGlosaAnalisis(String GlosaAnalisis) {
        this.GlosaAnalisis = GlosaAnalisis;
    }

    public ArrayList<Movimientos> getMovimientos() {
        return Movimientos;
    }

    public void setMovimientos(ArrayList<Movimientos> Movimientos) {
        this.Movimientos = Movimientos;
    }

    public String getValorComprobante() {
        return ValorComprobante;
    }

    public void setValorComprobante(String ValorComprobante) {
        this.ValorComprobante = ValorComprobante;
    }

   
   
    
}
