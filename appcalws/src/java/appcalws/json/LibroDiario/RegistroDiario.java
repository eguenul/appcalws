/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appcalws.json.LibroDiario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author esteban
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {    
    "FechaContable",
    "CantidadComprobantes",
    "CantidadMovimientos",
    "SumaValorComprobante"
    
})

public class RegistroDiario {
    
@XmlElement(name = "FechaContable", required=true)      
private String FechaContable;
@XmlElement(name = "CantidadComprobantes", required=true)      
private String CantidadComprobantes;
@XmlElement(name = "CantidadMovimientos", required=true)      
private String CantidadMovimientos;
@XmlElement(name = "SumaValorComprobante", required=true)      
private String SumaValorComprobante;

    public String getFechaContable() {
        return FechaContable;
    }

    public void setFechaContable(String FechaContable) {
        this.FechaContable = FechaContable;
    }

    public String getCantidadComprobantes() {
        return CantidadComprobantes;
    }

    public void setCantidadComprobantes(String CantidadComprobantes) {
        this.CantidadComprobantes = CantidadComprobantes;
    }

    public String getCantidadMovimientos() {
        return CantidadMovimientos;
    }

    public void setCantidadMovimientos(String CantidadMovimientos) {
        this.CantidadMovimientos = CantidadMovimientos;
    }

    public String getSumaValorComprobante() {
        return SumaValorComprobante;
    }

    public void setSumaValorComprobante(String SumaValorComprobante) {
        this.SumaValorComprobante = SumaValorComprobante;
    }

}
